diy-VT100 firmware
==================

Source code of diy-VT100 firmware.
Run on STM32F767VxTx.

URL: https://www.madresistor.com/diy-vt100
Hardware design: https://gitlab.com/madresistor/diy-vt100-hardware

If you are looking for the last tested design,
checkout tag "manuf-27-march-2017" from firmware repo and hardware design repo.

Dependencies
============

* unicore-mx

Installation
============
 make init   # only once
 make
 make flash  # flash via OpenOCD

Usage
=====

1. MicroPython

https://docs.micropython.org/en/latest/pyboard/pyboard/quickref.html#general-board-control

``` main.py
import pyb
pyb.repl_uart(pyb.UART(1, 9600)) # duplicate REPL on UART(1)
```

2. GNU/Linux (and other UNIX)

For reference, please see Arch GNU/Linux wiki.
https://wiki.archlinux.org/index.php/Working_with_the_serial_console

Using `systemctrl`
```
#systemctl start getty@ttyS0.service
```

Manually do the work
```
# sudo agetty /dev/ttyS0 9600 vt100
```

Assuming ttyS0 is the serial interface.
If you are using a USB-UART bridge, something ttyACM0 or ttyUSB0 you probably need to use.
9600 is the communcation speed.

Source map
==========

src/ -> firmware source code
libs/ -> external libraries
extra/openocd.cfg -> OpenOCD ready to use configuration
extra/stm32f767.ld -> STM32F767VGTx Linker file

Licence
=======

GNU/GPLv3 or later (see COPYING)

Getting help
============

get on to #diy-vt100 on IRC freenode
and report bug on https://gitlab.com/madresistor/diy-vt100-firmware

Getting involved
================

get on to #diy-vt100 on IRC freenode
and report bug on https://gitlab.com/madresistor/diy-vt100-firmware

Credits and references
======================

https://www.madresistor.com/

Maintainer
==========

Kuldeep Singh Dhaka <kuldeep@madresistor.com>
