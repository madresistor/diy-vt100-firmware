import png
import sys

r = png.Reader(sys.argv[1])
w, h, data, meta = r.read()
p = r.palette()

data = list(data)
p = list(p)

print("const uint8_t splash_data[%i][%i] = {" % (h, w))
for i in range(h):
	hex_arr = [hex(data[i][j]) for j in range(w)]
	print("{", ", ".join(hex_arr), "},")
print("};")
print()

def to_uint32(i, v):
	return "0x%s%s%s%s" % (format(i, '02X'), format(v[0], '02X'), \
		format(v[1], '02X'), format(v[2], '02X'))


print("const uint32_t splash_clut[%i] = {" % len(p))
hex_arr = [to_uint32(i, p[i]) for i in range(len(p))]
print(", ".join(hex_arr))
print("};")
