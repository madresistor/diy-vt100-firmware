/*
 * This file is part of diy-VT100.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bell.h"
#include "vt100.h"

void bell_keyclick(void)
{
	if (vt100.KEY_CLICK && !vt100.KBD_LOCKED) {
		hw_bell(VT100_BELL_SHORT);
	}
}

void bell_margin(void)
{
	if ((vt100.cursor.col + 8) != vt100.margin.right) {
		return;
	}

	if (vt100.MARGIN_BELL) {
		hw_bell(VT100_BELL_LONG);
	}
}

void bell_code_recv(void)
{
	hw_bell(VT100_BELL_LONG);
}
