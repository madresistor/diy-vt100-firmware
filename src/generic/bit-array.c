#include "bit-array.h"

inline uint8_t mask(unsigned index) { return 1 << (index & 0x7); }
inline unsigned offset(unsigned index) { return index >> 3; }

/* TODO: Use ARM bitband region */

void bitarr_high(uint8_t *data, unsigned index)
{
	data[offset(index)] |= mask(index);
}

void bitarr_low(uint8_t *data, unsigned index)
{
	data[offset(index)] &= ~mask(index);
}

void bitarr_write(uint8_t *data, unsigned index, bool value)
{
	uint8_t m = mask(index);
	unsigned o = offset(index);

	if (value) {
		data[o] |= m;
	} else {
		data[o] &= ~m;
	}
}

bool bitarr_read(const uint8_t *data, unsigned index)
{
	return !!(data[offset(index)] & mask(index));
}

void bitarr_flip(uint8_t *data, unsigned index)
{
	data[offset(index)] ^= mask(index);
}
