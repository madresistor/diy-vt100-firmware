/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt100.h"
#include "bell.h"
#include "uart.h"
#include "setup.h"
#include "misc.h"
#include "screen.h"

/* TODO:
 *  - send arrow keys
 *  - add support for CTRL+BREAK to send answer back string
 *  - support for DEL character
 */

#define KBD_MODIFIER HW_PRIV1
#define KBD_BREAK HW_PRIV2

static void ps2kbd_other_setup(uint8_t ch)
{
	switch (ch) {
	case '0':
		vt100_reset();
	break;
	case '2':
		setup_TAB_flip();
	break;
	case '3':
		setup_TABS_clear();
	break;
	case '4':
		setup_LOCAL();
	break;
	case '5':
		setup_switch();
	break;
	case '6':
		setup_value_flip();
	break;
	case '7':
		setup_uart_tx();
	break;
	case '8':
		setup_uart_rx();
	break;
	case '9':
		setup_DECCOLM();
	break;
	case ' ':
		setup_cursor_next();
	break;
	case 's':
		if (vt100.KBD_SHIFT) {
			setup_save();
		} else {
			return;
		}
	break;
	case 'r':
		if (vt100.KBD_SHIFT) {
			setup_recall();
		} else {
			return;
		}
	break;
	default:
	return;
	}

	bell_keyclick();
}

static void ps2kbd_f1(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_pfn(KEYPAD_PF1);
	}

	bell_keyclick();
}

static void ps2kbd_f2(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_pfn(KEYPAD_PF2);
	}

	bell_keyclick();
}

static void ps2kbd_f3(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_pfn(KEYPAD_PF3);
	}

	bell_keyclick();
}

static void ps2kbd_f4(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_pfn(KEYPAD_PF4);
	}

	bell_keyclick();
}

/* working as SETUP key */
static void ps2kbd_f5(void)
{
	setup();
	bell_keyclick();
}

static void ps2kbd_caps(void)
{
	/* caps status modify */
	if (!vt100.KBD_BREAK) {
		vt100.KBD_CAPS ^= true;
		bell_keyclick();
	}
}

static void ps2kbd_shift(void)
{
	vt100.KBD_SHIFT = vt100.KBD_BREAK;
}

static void ps2kbd_ctrl(void)
{
	vt100.KBD_CTRL = vt100.KBD_BREAK;
}

static void __ps2kbd_mux_keypad_num_arrow(uint8_t num, uint8_t arrow)
{
	if (vt100.KBD_MODIFIER) {
		/* arrow key pressed */
		uart_send_arrow(arrow);
	} else {
		/* send accoring to Keypad mode */
		uart_send_keypad_num(num);
	}
}

static void ps2kbd_keypad_0(void)
{
	if (vt100.SETUP_SHOW) {
		uart_send_keypad_num(0);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_1(void)
{
	if (vt100.SETUP_SHOW) {
		uart_send_keypad_num(1);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_2(void)
{
	if (vt100.SETUP_SHOW) {
		screen_brightness_dec();
	} else {
		__ps2kbd_mux_keypad_num_arrow(2, ARROW_DOWN);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_3(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(3);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_4(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_prev();
	} else {
		__ps2kbd_mux_keypad_num_arrow(4, ARROW_LEFT);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_5(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(5);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_6(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_next();
	} else {
		__ps2kbd_mux_keypad_num_arrow(6, ARROW_RIGHT);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_7(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(7);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_8(void)
{
	if (vt100.SETUP_SHOW) {
		screen_brightness_inc();
	} else {
		__ps2kbd_mux_keypad_num_arrow(8, ARROW_UP);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_9(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(9);
	}

	bell_keyclick();
}

static void ps2kbd_keypad_dash(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_dash();
	}

	bell_keyclick();
}

static void ps2kbd_keypad_dot(void)
{
	if (!vt100.SETUP_SHOW) {
		if (vt100.KBD_MODIFIER) {
			uart_send(ASCII_DEL);
		} else {
			uart_send_keypad_dot();
		}
	}

	bell_keyclick();
}

static void ps2kbd_enter(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_first();
	} else {
		if (vt100.KBD_MODIFIER) {
			uart_send_keypad_enter();
		} else {
			uart_send_return();
		}
	}

	bell_keyclick();
}

static void ps2kbd_backspace(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send(ASCII_BS);
	}

	bell_keyclick();
}

static void ps2kbd_escape(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send(ASCII_ESCAPE);
	}

	bell_keyclick();
}

static void ps2kbd_tab(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_tab();
	} else {
		uart_send(ASCII_TAB);
	}

	bell_keyclick();
}

static void ps2kbd_scroll(void)
{
	if (vt100.SETUP_SHOW) {
		return;
	}

	if (vt100.XOFF_SEND) {
		uart_send(ASCII_XON);
		vt100.XOFF_SEND = false;
	} else {
		uart_send(ASCII_XOFF);
		vt100.XOFF_SEND = true;
	}

	vt100.XOFF_SCROLL = true;
}

typedef void (*_scan_code_callback)(void);

struct _scan_code {
	/*
	 * 0 = do not exists
	 * 1 = character is affected by caps status
	 * other = call
	 */
	_scan_code_callback callback;

	/* 0:main, 1:replacement */
	uint8_t ch[2];
};

#define SCAN_CODE_SIZE 127
#define SCI_TYPE_NUMBER (NULL + 2)
#define SCI_TYPE_SPECIAL (NULL + 2)
#define SCI_TYPE_ALPHABET (NULL + 1)
#define SCI_TYPE_IGNORE NULL
#define IS_VALID_CALLBACK(fn) ((fn) > (_scan_code_callback)(NULL + 2))

#define SCI_NUMBER(ch, ch_alt)		{SCI_TYPE_NUMBER, {ch, ch_alt}}
#define SCI_SPECIAL(ch, ch_alt)		{SCI_TYPE_SPECIAL, {ch, ch_alt}}
#define SCI_ALPHABET(ch, ch_alt)	{SCI_TYPE_ALPHABET, {ch, ch_alt}}
#define SCI_CALLBACK(cb)			{cb, {0}}
#define SCI_IGNORE					{SCI_TYPE_IGNORE}

/* require break */
#define SCI_CALLBACK_RB(cb)			{cb, {1}}

static const struct _scan_code scan_code[SCAN_CODE_SIZE] = {
	SCI_IGNORE,
	SCI_IGNORE, /* KEYBOARD_PS2_F9 */
	SCI_IGNORE,
	SCI_CALLBACK(ps2kbd_f5), /* KEYBOARD_PS2_F5 */
	SCI_CALLBACK(ps2kbd_f3),
	SCI_CALLBACK(ps2kbd_f1),
	SCI_CALLBACK(ps2kbd_f2),
	SCI_IGNORE, /* KEYBOARD_PS2_F12 */
	SCI_IGNORE,
	SCI_IGNORE, /* KEYBOARD_PS2_F10 */
	SCI_IGNORE, /* KEYBOARD_PS2_F8 */
	SCI_IGNORE, /* KEYBOARD_PS2_F6 */
	SCI_CALLBACK(ps2kbd_f4),
	SCI_CALLBACK(ps2kbd_tab),//SCI_SPECIAL	(ASCII_TAB, ASCII_TAB),
	SCI_SPECIAL	('`', '~'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE, /* left alt */
	SCI_CALLBACK_RB(ps2kbd_shift), /* left shift */
	SCI_IGNORE,
	SCI_CALLBACK_RB(ps2kbd_ctrl), /* left ctrl */
	SCI_ALPHABET('q', 'Q'),
	SCI_NUMBER	('1', '!'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_ALPHABET('z', 'Z'),
	SCI_ALPHABET('s', 'S'),
	SCI_ALPHABET('a', 'A'),
	SCI_ALPHABET('w', 'W'),
	SCI_NUMBER	('2', '@'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_ALPHABET('c', 'C'),
	SCI_ALPHABET('x', 'X'),
	SCI_ALPHABET('d', 'D'),
	SCI_ALPHABET('e', 'E'),
	SCI_NUMBER	('4', '$'),
	SCI_NUMBER	('3', '#'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_ALPHABET(' ', ' '),
	SCI_ALPHABET('v', 'V'),
	SCI_ALPHABET('f', 'F'),
	SCI_ALPHABET('t', 'T'),
	SCI_ALPHABET('r', 'R'),
	SCI_NUMBER	('5', '%'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_ALPHABET('n', 'N'),
	SCI_ALPHABET('b', 'B'),
	SCI_ALPHABET('h', 'H'),
	SCI_ALPHABET('g', 'G'),
	SCI_ALPHABET('y', 'Y'),
	SCI_NUMBER	('6', '^'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_ALPHABET('m', 'M'),
	SCI_ALPHABET('j', 'J'),
	SCI_ALPHABET('u', 'U'),
	SCI_NUMBER	('7', '&'),
	SCI_NUMBER	('8', '*'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_SPECIAL	(',', '<'),
	SCI_ALPHABET('k', 'K'),
	SCI_ALPHABET('i', 'I'),
	SCI_ALPHABET('o', 'O'),
	SCI_NUMBER	('0', ')'),
	SCI_NUMBER	('9', '('),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_SPECIAL	('.', '>'),
	SCI_SPECIAL	('/', '?'),
	SCI_ALPHABET('l', 'L'),
	SCI_SPECIAL	(';', ':'),
	SCI_ALPHABET('p', 'P'),
	SCI_SPECIAL	('-', '_'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_SPECIAL	('\'', '"'),
	SCI_IGNORE,
	SCI_SPECIAL	('[', '{'),
	SCI_SPECIAL	('=', '+'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_CALLBACK_RB(ps2kbd_caps),
	SCI_CALLBACK_RB(ps2kbd_shift), /* right shift */
	SCI_CALLBACK(ps2kbd_enter),
	SCI_SPECIAL	(']', '}'),
	SCI_IGNORE,
	SCI_SPECIAL	('\\', '|'),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_CALLBACK(ps2kbd_backspace),//SCI_SPECIAL	(ASCII_BS, ASCII_BS),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_CALLBACK(ps2kbd_keypad_1),
	SCI_IGNORE,
	SCI_CALLBACK(ps2kbd_keypad_4),
	SCI_CALLBACK(ps2kbd_keypad_7),
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_CALLBACK(ps2kbd_keypad_0),
	SCI_CALLBACK(ps2kbd_keypad_dot),
	SCI_CALLBACK(ps2kbd_keypad_2),
	SCI_CALLBACK(ps2kbd_keypad_5),
	SCI_CALLBACK(ps2kbd_keypad_6),
	SCI_CALLBACK(ps2kbd_keypad_8),
	SCI_CALLBACK(ps2kbd_escape),//SCI_SPECIAL	(ASCII_ESCAPE, ASCII_ESCAPE),
	SCI_IGNORE, /* num lock */
	SCI_IGNORE, /* KEYBOARD_PS2_F11 */
	SCI_SPECIAL	('+', '+'),
	SCI_CALLBACK(ps2kbd_keypad_3),
	SCI_CALLBACK(ps2kbd_keypad_dash),
	SCI_SPECIAL	('*', '*'),
	SCI_CALLBACK(ps2kbd_keypad_9),
	SCI_CALLBACK(ps2kbd_scroll)
//	SCI_IGNORE,
//	SCI_IGNORE,
//	SCI_IGNORE,
//	SCI_IGNORE,
//	SCI_IGNORE, /* KEYBOARD_PS2_F7 */
};

void ps2kbd_decode(uint8_t data)
{
	if (data >= SCAN_CODE_SIZE) {
		switch (data) {
		case 0xF0:
			/* some key released */
			vt100.KBD_BREAK = true;
		break;
		case 0xE0:
			vt100.KBD_MODIFIER = true;
		break;
		}

		return;
	}

	const struct _scan_code *item = &scan_code[data];

	if (IS_VALID_CALLBACK(item->callback)) {
		if (!vt100.KBD_BREAK || item->ch[0]) {
			/* continue if break is reset or explicitly required */
			item->callback();
		}

		return;
	}

	if (item->callback == SCI_TYPE_IGNORE) {
		/* easy, just do nothing */
		return;
	}

	if (vt100.KBD_BREAK) {
		/* There is a break. do not process the character */
		goto done;
	}

	if (vt100.SETUP_SHOW) {
		/* setup is being shown. it should be passed to setup */
		ps2kbd_other_setup(item->ch[0]);
		goto done;
	}

	if (vt100.KBD_CTRL) {
		/* Ctrl key is being pressed.
		 *  Instead of processing it as normal, it should be sent
		 *    as control character. */
		uint8_t ch = item->ch[1];
		switch (ch) {
		case '{':
			ch = '[';
		break;
		case '|':
			ch = '\\';
		break;
		case '}':
			ch = ']';
		break;
		}

		if (ch >= '@' && ch <= '_') {
			uart_send(ch - '@');
			bell_keyclick();
		}

		goto done;
	}

	uint8_t ch_alt = 0;

	if (item->callback == SCI_TYPE_ALPHABET && vt100.KBD_CAPS) {
		/* Caps is being pressed and the character is affected by CAPS
		 * The case if set to uppercase (ie selected the alternate version
		 */
		ch_alt |= 1;
	}

	if (vt100.KBD_SHIFT) {
		/* Shift is being pressed, invert the case */
		ch_alt ^= 1;
	}

	uart_send(item->ch[ch_alt]);
	bell_keyclick();

	done:
	vt100.KBD_BREAK = false;
	vt100.KBD_MODIFIER = false;
}
