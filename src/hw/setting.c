/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt100.h"
#include "common.h"
#include "uart.h"
#include "eeprom.h"
#include "debug.h"
#include <string.h>
#include "delay.h"

/* Offset to apply before writing/reading from EEPROM. */
#define EEPROM_OFFSET 0x00

/*
 * value = bps/(10 * clkmul_conv)
 *   where clkmul_conv = 16 for clkmul == 1
 *   where clkmul_conv = 64 for clkmul == 2
 */
const struct _vt100_uart_speed vt100_uart_speed[] = {
	{"1200", "64", '1'},
	{"1800", "72", '1'},
	{"2000", "80", '1'},
	{"2400", "88", '1'},
	{"3600", "96", '1'},
	{"4800", "104", '1'},
	{"9600", "112", '1'},
	{"14400", "90", '1'},
	{"19200", "120", '1'},
	{"28800", "180", '1'},
	{"38400", "240", '1'},
	{"56000", "350", '1'},
	{"57600", "90", '2'},
	{"115200", "180", '2'},
	{"128000", "200", '2'},
	{"153600", "240", '2'},
	{"230400", "360", '2'},
	{"256000", "400", '2'},
	{"460800", "720", '2'},
	{"921600", "1440", '2'},
	{NULL}
};

extern const uint32_t uart_baudrate[];

PLACE_IN_DTCM_DATA
const struct _vt100_uart_speed
	*vt100_uart_rx_speed = vt100_uart_speed,
	*vt100_uart_tx_speed = vt100_uart_speed;

struct _eeprom_layout {
	uint8_t version; /* = 0x01 ie 0.1  */;

	uint8_t brightness; /* value between 0% and 100% (0 - 100) only */

	uint8_t answerback_len; /* = ANSWERBACK_SIZE */
	uint8_t answerback[ANSWERBACK_SIZE];

	uint32_t uart_rx, uart_tx;

	uint8_t col /*  = SCREEN_COL */;
	uint8_t tab_stop[BYTES_FOR_1BIT_COLUMN];

	uint8_t feature_count; /* = 16 */
	struct {
		uint8_t DECANM:1;
		uint8_t DECARM:1;
		uint8_t AUTOX:1;
		uint8_t BPC:1;
		uint8_t DECCOLM:1;
		uint8_t CURSOR:1;
		uint8_t DECINLM:1;
		uint8_t LNM:1;
		uint8_t KEY_CLICK:1;
		uint8_t MARGIN_BELL:1;
		uint8_t PARITY:1;
		uint8_t PARITY_SENSE:1;
		uint8_t DECSCNM:1;
		uint8_t DECSCLM:1;
		uint8_t DECAWM:1;
		uint8_t SHIFTED:1;
		uint8_t POWER:1;
	} feature; /* setup B */
} __attribute__((packed));

const struct _eeprom_layout flash_fallback = {
	.version = 0x10,

	.answerback_len = ANSWERBACK_SIZE,
	.answerback = "im a diy-VT100",

	.brightness = SCREEN_BRIGHTNESS_DEFAULT,

	.uart_rx = UART_SPEED_DEFAULT,
	.uart_tx = UART_SPEED_DEFAULT,

	.col = SCREEN_COL,

	.feature_count = 16,
	.feature = {
		.DECSCLM = true,
		.DECARM = true,
		.DECARM = true,
		.KEY_CLICK = true,
		.AUTOX = true,
		.BPC = true
	}
};

void setting_to_eeprom(struct _eeprom_layout *data)
{
	data->version = 0x01;

	data->answerback_len = ANSWERBACK_SIZE;
	memcpy(data->answerback, (void *) vt100.answerback, ANSWERBACK_SIZE);

	data->brightness = vt100.brightness;

	data->col = SCREEN_COL;
	memcpy(data->tab_stop, (void *) vt100.tab_stop, BYTES_FOR_1BIT_COLUMN);

	data->uart_tx = uart_baudrate[vt100.uart_tx];
	data->uart_rx = uart_baudrate[vt100.uart_rx];

#define FEATURE_SETTING_TO_EEPROM(bit_name) \
	data->feature.bit_name = vt100.bit_name

	data->feature_count = 16;
	FEATURE_SETTING_TO_EEPROM(DECANM);
	FEATURE_SETTING_TO_EEPROM(DECARM);
	FEATURE_SETTING_TO_EEPROM(AUTOX);
	FEATURE_SETTING_TO_EEPROM(BPC);
	FEATURE_SETTING_TO_EEPROM(DECCOLM);
	FEATURE_SETTING_TO_EEPROM(CURSOR);
	FEATURE_SETTING_TO_EEPROM(DECINLM);
	FEATURE_SETTING_TO_EEPROM(LNM);
	FEATURE_SETTING_TO_EEPROM(KEY_CLICK);
	FEATURE_SETTING_TO_EEPROM(MARGIN_BELL);
	FEATURE_SETTING_TO_EEPROM(PARITY);
	FEATURE_SETTING_TO_EEPROM(PARITY_SENSE);
	FEATURE_SETTING_TO_EEPROM(DECSCNM);
	FEATURE_SETTING_TO_EEPROM(DECSCLM);
	FEATURE_SETTING_TO_EEPROM(DECAWM);
	FEATURE_SETTING_TO_EEPROM(SHIFTED);
	FEATURE_SETTING_TO_EEPROM(POWER);
}

void hw_setting_store(void)
{
	struct _eeprom_layout data;

	setting_to_eeprom(&data);

	uint8_t total_len = sizeof(data), written_len = 0;

	eeprom_write_protect(false);

	/*
	 * PAGE WRITE
	 * Only upto 8byte can be written at a time
	 */
	while (written_len < total_len) {
		uint8_t len = total_len - written_len;
		if (len > 8) {
			len = 8;
		}

		const uint8_t *_data = ((uint8_t *) &data) + written_len;
		uint8_t actual = eeprom_write(EEPROM_OFFSET + written_len, _data, len);
		written_len += actual;

		if (actual < len) {
			LOG_LN("Problem, partial data only written to EEPROM");
			break;
		}

		delay(DELAY_RESOL_1MS, 5); /* 5ms */
	}

	eeprom_write_protect(true);

	if (!written_len) {
		LOG_LN("nothing written to EEPROM");
		/* FIXME: write the setting to flash "flash_fallback" */
	} else if (written_len < total_len) {
		LOGF_LN("failed to write full data. only %"PRIu8" writte out of %"PRIu8" bytes",
			written_len, total_len);
	}
}

void eeprom_to_setting(const struct _eeprom_layout *data)
{
	if (data->version != 0x01) {
		LOG_LN("EEPROM data that is not of version 0.1");
	}

	if (data->answerback_len == ANSWERBACK_SIZE) {
		memcpy((void *) vt100.answerback, data->answerback, ANSWERBACK_SIZE);
	} else {
		LOG_LN("answerback string length mismatch");
		memset((void *) vt100.answerback, 0, ANSWERBACK_SIZE);
	}

	if (data->brightness <= 100) {
		vt100.brightness = data->brightness;
	} else {
		LOG_LN("brightness value out of 0-100 range");
		vt100.brightness = SCREEN_BRIGHTNESS_DEFAULT;
	}

	if (data->col == SCREEN_COL) {
		memcpy((void *) vt100.tab_stop, data->tab_stop, BYTES_FOR_1BIT_COLUMN);
	} else {
		LOG_LN("number of column mismatch");
		memset((void *) vt100.tab_stop, 0, BYTES_FOR_1BIT_COLUMN);
	}

#define FEATURE_EEPROM_TO_SETTING(bit_name) \
	vt100.bit_name = data->feature.bit_name

	if (data->feature_count == 16) {
		FEATURE_EEPROM_TO_SETTING(DECANM);
		FEATURE_EEPROM_TO_SETTING(DECARM);
		FEATURE_EEPROM_TO_SETTING(AUTOX);
		FEATURE_EEPROM_TO_SETTING(BPC);
		FEATURE_EEPROM_TO_SETTING(DECCOLM);
		FEATURE_EEPROM_TO_SETTING(CURSOR);
		FEATURE_EEPROM_TO_SETTING(DECINLM);
		FEATURE_EEPROM_TO_SETTING(LNM);
		FEATURE_EEPROM_TO_SETTING(KEY_CLICK);
		FEATURE_EEPROM_TO_SETTING(MARGIN_BELL);
		FEATURE_EEPROM_TO_SETTING(PARITY);
		FEATURE_EEPROM_TO_SETTING(PARITY_SENSE);
		FEATURE_EEPROM_TO_SETTING(DECSCNM);
		FEATURE_EEPROM_TO_SETTING(DECSCLM);
		FEATURE_EEPROM_TO_SETTING(DECAWM);
		FEATURE_EEPROM_TO_SETTING(SHIFTED);
		FEATURE_EEPROM_TO_SETTING(POWER);
	} else {
		LOG_LN("feature count mismatch");
	}

	uint32_t uart_speed;
	if (data->uart_rx == data->uart_tx) {
		uart_speed = data->uart_tx;
	} else {
		LOGF_LN("UART TX: %"PRIu32 ", RX: %"PRIu32,
			data->uart_tx, data->uart_rx);
		LOG_LN("UART speed rx and tx mismatch");
		uart_speed = UART_SPEED_DEFAULT;
	}

	unsigned i;

	search_speed:
	for (i = 0; uart_baudrate[i] > 0; i++) {
		if (uart_baudrate[i] == uart_speed) {
			break;
		}
	}

	if (uart_baudrate[i]) {
		vt100.uart_tx = vt100.uart_rx = i;
	} else {
		LOG_LN("no uart speed found for the give value in eeprom");
		uart_speed = UART_SPEED_DEFAULT;
		goto search_speed;
	}
}

void hw_setting_load(void)
{
	struct _eeprom_layout data;
	const struct _eeprom_layout *res;

	uint8_t total_len = sizeof(data);
	uint8_t readed_len = eeprom_read(EEPROM_OFFSET, (uint8_t *) &data, total_len);

	if (!readed_len) {
		LOG_LN("Nothing readed from EEPROM");
		res = &flash_fallback;
	} else if (readed_len < total_len) {
		LOGF_LN("failed to read full data. only %"PRIu8" read out of %"PRIu8" bytes",
			readed_len, total_len);
		res = &data;
	} else {
		res = &data;
	}

	eeprom_to_setting(res);
}
