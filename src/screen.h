/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIY_VT100_SCREEN_H
#define DIY_VT100_SCREEN_H

#include "common.h"

__BEGIN_DECLS

#include "vt100.h"

/* [LCD] */
void screen_invert(bool);

void screen_shiftup(void);
void screen_shiftdown(void);


void hw_screen_brightness(uint8_t);

/**
 * Decrease screen brightness
 */
void screen_brightness_dec(void);

/**
 * Increase screen brightness
 */
void screen_brightness_inc(void);

__END_DECLS

#endif
