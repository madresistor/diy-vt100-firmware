/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VT100_CHAR_H
#define VT100_CHAR_H

#include "common.h"

/* when parity error occurs , hardware will send
 * place a checkerboard character ascii(176) */
void vt100_putch(uint8_t);

void vt100_DECALN(void);

void vt100_LF(void);
void vt100_NEL(void);
void vt100_CR(void);

void vt100_ED(void);
void vt100_EL(void);

#endif
