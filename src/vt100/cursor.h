/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VT100_CURSOR_H
#define VT100_CURSOR_H

#include "common.h"
#include "screen.h"

void vt100_BS(void);

void vt100_CUP(void);
void vt100_CUD(void);
void vt100_CUU(void);
void vt100_CUF(void);
void vt100_CUB(void);
void vt100_DECRC(void);
void vt100_DECSC(void);

void vt100_RI(void);
void vt100_IND(void);

#endif
